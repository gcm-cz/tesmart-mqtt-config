FROM registry.srv.gcm.cz/baseimage/python:3.11
WORKDIR /app

ADD requirements.txt /
RUN pip install -r /requirements.txt
ADD src/* .
ENTRYPOINT ["/app/tesmart_controller.py"]
