#!/usr/bin/env python3

import asyncio
import json
import sys
import logging
import signal
from asyncio import CancelledError

from os import getenv
from dataclasses import dataclass
from typing import Optional, Dict, Callable, Awaitable, Tuple
from argparse import ArgumentParser
from enum import Enum
from contextlib import AsyncExitStack

from serial import SerialException
from serial_asyncio import create_serial_connection
from asyncio_mqtt import Client, MqttError, Will
from paho.mqtt.client import MQTTMessage
import serial
import re


class SerialParity(str, Enum):
    NONE = "N"
    EVEN = "E"
    ODD = "O"
    MARK = "M"
    SPACE = "S"

    def to_serial(self):
        if self is SerialParity.NONE:
            return serial.PARITY_NONE

        if self is SerialParity.EVEN:
            return serial.PARITY_EVEN

        if self is SerialParity.ODD:
            return serial.PARITY_ODD

        if self is SerialParity.MARK:
            return serial.PARITY_MARK

        if self is SerialParity.SPACE:
            return serial.PARITY_SPACE

        raise ValueError(f"SerialParity {self!r} cannot be converted to serial parity.")


@dataclass
class SerialConfig:
    port: str = "/dev/ttyUSB0"  # Serial port where TESmart HDMI Matrix is located
    baudrate: int = 9600  # Serial port baud rate in bits per second
    databits: int = 8  # Number of data bits
    parity: SerialParity = SerialParity.NONE  # Parity configuration
    stopbits: float = 1  # Number of stop bits

    def __post_init__(self):
        if self.databits not in (5, 6, 7, 8):
            raise ValueError("SerialConfig.databits has unknown value.")

        if self.stopbits not in (1, 1.5, 2):
            raise ValueError("SerialConfig.stopbits has unknown value.")

    @property
    def databits_to_serial(self):
        if self.databits == 5:
            return serial.FIVEBITS

        if self.databits == 6:
            return serial.SIXBITS

        if self.databits == 7:
            return serial.SEVENBITS

        if self.databits == 8:
            return serial.EIGHTBITS

        raise ValueError(f"Databits {self.databits} cannot be converted to serial databits.")

    @property
    def stopbits_to_serial(self):
        if self.stopbits == 1:
            return serial.STOPBITS_ONE

        if self.stopbits == 1.5:
            return serial.STOPBITS_ONE_POINT_FIVE

        if self.stopbits == 2:
            return serial.STOPBITS_TWO


@dataclass
class MqttConfig:
    host: str = "localhost"
    port: int = 1883
    user: Optional[str] = None
    password: Optional[str] = None
    client_id: Optional[str] = None
    topic: str = "tesmart"
    stat_interval: int = 60

    lwt_online: str = "Online"
    lwt_offline: str = "Offline"
    
    @property
    def lwt_topic(self) -> str:
        return f"tele/{self.topic}/LWT"


@dataclass
class Config:
    serial: SerialConfig
    mqtt: MqttConfig
    debug: bool = False


class TeSmartSerial:
    def __init__(self, serial_config: SerialConfig, loop: asyncio.AbstractEventLoop = None,
            inputs: int = 4, outputs: int = 4):
        self.serial_config = serial_config

        self.reader: Optional[StreamReader] = None
        self.writer: Optional[StreamWriter] = None
        self.transport = None

        self.loop = loop or asyncio.get_event_loop()
        self.serial_open = asyncio.Event()

        self.inputs = inputs
        self.outputs = outputs

        self.logger = logging.getLogger("tesmart.serial")

        self.io_state_received = asyncio.Event()
        self._reset_io_state()
        self.on_io_state = []

    async def _open_port(self) -> None:
        try:
            while True:
                try:
                    self.reader = asyncio.StreamReader(loop=self.loop)
                    protocol = asyncio.StreamReaderProtocol(self.reader, loop=self.loop)

                    self.logger.info(f"Opening serial port {self.serial_config.port}, {self.serial_config.baudrate} bps, {self.serial_config.stopbits} stopbits, {self.serial_config.databits} databits, {self.serial_config.parity} parity...")
                    self.transport, _ = await create_serial_connection(
                        self.loop,
                        lambda: protocol,
                        self.serial_config.port,
                        baudrate=self.serial_config.baudrate,
                        bytesize=self.serial_config.databits_to_serial,
                        parity=self.serial_config.parity.to_serial(),
                        stopbits=self.serial_config.stopbits_to_serial
                    )

                    self.writer = asyncio.StreamWriter(self.transport, protocol, self.reader, self.loop)

                    original_writer = self.writer.write
                    def _logging_writer(write_bytes: bytes):
                        self.logger.debug(f"Serial output: {write_bytes}")
                        return original_writer(write_bytes)

                    self.writer.write = _logging_writer

                    self.logger.info("Serial port opened!")
                    self.serial_open.set()
                    return
                except SerialException as exc:
                    self.logger.error(str(exc))

                await asyncio.sleep(1)
        except CancelledError:
            self.logger.error("Bailing out.")
            raise
        except Exception as exc:
            self.logger.exception(exc)

    def _reset_io_state(self):
        self.io_state: Dict[int, Optional[int]] = {
            x: None
            for x in range(1, self.outputs + 1)
        }
        self.io_state_received.clear()

    async def wait_for_io_state(self):
        await self.io_state_received.wait()

    async def _handle_reads(self):
        link_state_re = re.compile(r"LINK:(O[0-9]I[0-9];)+END")
        one_state_re = re.compile(r"O([0-9])I([0-9])")

        self.logger.debug("Started serial read task.")

        try:
            data_buffer = b""
            while not self.reader.at_eof():
                data_buffer += await self.reader.read(1024)
                if link_state_re.search(data_buffer.decode()):
                    for output_no, input_no in one_state_re.findall(data_buffer.decode()):
                        if int(output_no) in self.io_state:
                            self.io_state[int(output_no)] = int(input_no)

                    if all(map(lambda x: x is not None, self.io_state.values())):
                        self.io_state_received.set()
                        self.logger.debug(f"Received I/O state: {self.io_state}")
                        for cb in self.on_io_state:
                            try:
                                await cb()
                            except Exception as exc:
                                logging.exception(exc)
                    else:
                        self.io_state_received.clear()

                    data_buffer = b""
                elif data_buffer.endswith(b"\r\n"):
                    self.logger.debug(f"Unknown data received: {data_buffer}")
                    data_buffer = b""
        except Exception as exc:
            raise
        finally:
            self.logger.debug("Serial read task finished.")

    async def get_outputs_state(self) -> Dict[int, int]:
        await self.serial_open.wait()

        self.logger.debug("Requesting output state from TeSmart Matrix.")
        self._reset_io_state()
        self.writer.write("MT00RD0000NT\r\n".encode())
        await self.writer.drain()
        await self.wait_for_io_state()
        return self.io_state

    async def switch_output(self, output_no: int, input_no: int):
        await self.serial_open.wait()

        if output_no < 1 or output_no > self.outputs:
            raise ValueError(f"output_no must be between 1 and {self.outputs}.")

        if input_no < 1 or input_no > self.inputs:
            raise ValueError(f"input_no must be between 1 and {self.inputs}.")

        self.logger.debug(f"Switching output {output_no} to input {input_no}.")
        self.writer.write(f"MT00SW{input_no:02d}{output_no:02d}NT\r\n".encode())  # Switch output
        await self.writer.drain()

    async def start(self):
        while True:
            await self._open_port()

            try:
                self.reader_task = self.loop.create_task(self._handle_reads())
                await self.get_outputs_state()
                await self.reader_task
            except CancelledError:
                break
            except Exception as exc:
                self.logger.exception(exc)
                self.reader_task.cancel()
                self.serial_open.clear()
            finally:
                self.reader_task.cancel()
                try:
                    await self.reader_task
                except CancelledError:
                    pass

                if self.writer:
                    self.writer.close()

                if self.transport:
                    self.transport.close()

                self.logger.info("Serial port closed.")

class MqttClient:
    def __init__(self, mqtt_config: MqttConfig, loop: asyncio.AbstractEventLoop = None):
        self.logger = logging.getLogger("tesmart.mqtt")

        self.mqtt_config: MqttConfig = mqtt_config
        self.mqtt: Optional[Client] = None
        self.mqtt_connected = asyncio.Event()

        self.loop = loop or asyncio.get_event_loop()
        
        self.command_topic = f"cmnd/{mqtt_config.topic}/#"
        self.stat_topic = f"stat/{mqtt_config.topic}/"

        self.commands: Dict[str, Callable[[MQTTMessage], Awaitable[Optional[str]]]] = {}

    def create_client(self) -> Client:
        return Client(
            hostname=self.mqtt_config.host,
            port=self.mqtt_config.port,
            username=self.mqtt_config.user,
            password=self.mqtt_config.password,
            client_id=self.mqtt_config.client_id,
            will=Will(self.mqtt_config.lwt_topic, payload=self.mqtt_config.lwt_offline, retain=True),
        )

    def register_command(self, command: str, callback: Callable[[MQTTMessage], Awaitable[Optional[str]]]):
        if command.upper() in self.commands:
            raise AttributeError(f"There is already handle for command {command}.")

        self.commands[command.upper()] = callback

    async def start(self):
        while True:
            self.mqtt_connected.clear()
            self.mqtt = self.create_client()

            try:
                while True:
                    try:
                        self.logger.info("Connecting to MQTT broker...")
                        await self.mqtt.connect()
                        self.logger.info("Connected!")
                        await self.mqtt.publish(self.mqtt_config.lwt_topic, self.mqtt_config.lwt_online, retain=True)
                        self.mqtt_connected.set()
                        break
                    except MqttError as exc:
                        self.logger.error(str(exc))

                    await asyncio.sleep(1)
            except CancelledError:
                self.logger.error("Bailing out.")
                raise

            try:
                await self.mqtt.subscribe(self.command_topic)
                self.logger.info(f"Subscribed to {self.command_topic}.")

                while True:
                    async with self.mqtt.unfiltered_messages() as messages:
                        async for message in messages:  # type: MQTTMessage
                            command = message.topic[len(self.command_topic) - 1:].upper()
                            self.logger.debug(f"Received command {command} with payload {message.payload}")
                            if command in self.commands:
                                resp = await self.commands[command](message)
                                self.logger.debug(f"Command returned: {resp}")
                                if resp is not None:
                                    await self.publish_command_resp(command, resp)
            except MqttError as exc:
                self.logger.error(str(exc))

            except CancelledError:
                await self.mqtt.unsubscribe(self.command_topic)
                self.logger.info(f"Unsubscribed from {self.command_topic}.")

                await self.mqtt.publish(self.mqtt_config.lwt_topic, self.mqtt_config.lwt_offline)
                self.logger.debug("Published LWT Offline.")
                break

            finally:
                if not self.mqtt._disconnected.done():
                    try:
                        await self.mqtt.disconnect()
                    except MqttError as exc:
                        self.logger.exception(exc)
                        await self.mqtt.force_disconnect()
                else:
                    disc_exc = self.mqtt._disconnected.exception()
                    if disc_exc is not None:
                        self.logger.exception(disc_exc)

    async def publish_command_resp(self, command: str, payload: str):
        await self.mqtt_connected.wait()
        try:
            await self.mqtt.publish(f"{self.stat_topic}{command.upper()}", payload=payload.encode("utf-8"))
        except MqttError as exc:
            self.logger.exception(exc)


class TeSmartMqttController:
    def __init__(self, config: Config, loop: asyncio.AbstractEventLoop = None):
        self.loop = loop or asyncio.get_event_loop()
        self.logger = logging.getLogger("tesmart")

        self.config = config

        self.ts_serial = TeSmartSerial(config.serial, loop)
        self.ts_serial.on_io_state.append(self._on_io_state)

        self.mqtt_client = MqttClient(self.config.mqtt, loop=self.loop)
        self.mqtt_client.register_command("State", self.request_state)
        self.mqtt_client.register_command("Switch", self.switch_state)

        self.state_publisher = self.loop.create_task(self.state_publisher_task())
        self.state_publisher_event = asyncio.Event()

        self.stat_interval_task = self.loop.create_task(self.stat_interval_task_worker())
 
    async def request_state(self, _: Optional[MQTTMessage]=None) -> None:
        """
        Command handler to programatically request current state of outputs. This command does not
        need to return anything, because each state update is handled from state_publisher_task.
        """
        await self.ts_serial.get_outputs_state()

    async def state_publisher_task(self):
        """
        Each time state is updated, publish it to state topic.
        """
        while True:
            await self.state_publisher_event.wait()
            self.state_publisher_event.clear()

            await self.mqtt_client.publish_command_resp("state", json.dumps({"status": "OK", "outputs": self.ts_serial.io_state}))

    async def stat_interval_task_worker(self):
        """
        Periodically request output states from the matrix at rate of config.mqtt.stat_interval.
        """
        try:
            while True:
                await asyncio.sleep(self.config.mqtt.stat_interval)
                await self.ts_serial.get_outputs_state()
        except CancelledError:
            pass


    async def switch_state(self, message: MQTTMessage) -> str:
        """
        Switch state of outputs on request from MQTT.
        Payload: {"output": 1, "input": 2}
                 - switches output 1 to input 2.
        """
        try:
            req = json.loads(message.payload.decode("utf-8"))
            if "input" not in req:
                raise ValueError("Key input missing in payload.")

            if "output" not in req:
                raise ValueError("Key output missing in payload.")

            input_no = req["input"]
            output_no = req["output"]

            if input_no < 1 or input_no > self.ts_serial.inputs:
                raise ValueError("Input out of range.")

            if output_no < 1 or output_no > self.ts_serial.outputs:
                raise ValueError("Output out of range.")

            await self.ts_serial.switch_output(output_no, input_no)
            await self.ts_serial.get_outputs_state()
            return json.dumps({"status": "OK"})
        except ValueError as exc:
            return json.dumps({"status": "ERR", "error": str(exc)})

    async def _on_io_state(self):
        self.state_publisher_event.set()

    async def start(self):
        try:
            await asyncio.gather(
                self.mqtt_client.start(),
                self.ts_serial.start()
            )
        except CancelledError:
            self.state_publisher.cancel()
            self.stat_interval_task.cancel()

            try:
                await self.state_publisher
            except CancelledError:
                pass

        except Exception as exc:
            raise


async def app(config: Config) -> int:
    loop = asyncio.get_event_loop()

    controller = TeSmartMqttController(config, loop)
    quit_requested = False

    def set_quit():
        logging.info("Received quit request.")

        nonlocal quit_requested
        quit_requested = True

    loop.add_signal_handler(signal.SIGINT, set_quit)
    loop.add_signal_handler(signal.SIGTERM, set_quit)

    main_task = loop.create_task(controller.start())

    while not quit_requested:
        await asyncio.sleep(.1)
        if main_task.done():
            await main_task   # Collect potential exception from main task.
            break

    # Stop main task
    main_task.cancel()
    try:
        await main_task
    except CancelledError:
        pass

    logging.info("Application quit.")

    return 0


def get_config() -> Config:
    default_config = Config(serial=SerialConfig(), mqtt=MqttConfig())

    parser = ArgumentParser(sys.argv[0], add_help=False)

    parser.add_argument("--help", action="store_true", dest="help",
                        help="Print this message and exit.")

    parser.add_argument("--debug, -d", action="store_true", dest="debug",
                        help="Enable verbose logging.")

    serial_group = parser.add_argument_group("serial port")

    serial_group.add_argument("--port", dest="serial_port", type=str,
                              default=getenv("SERIAL_PORT", default_config.serial.port),
                              help="Serial port where TESmart HDMI Matrix is connected. ENV: SERIAL_PORT")

    serial_group.add_argument("--baudrate", "-b", dest="serial_baudrate", type=int,
                              default=getenv("SERIAL_BAUDRATE", SerialConfig.baudrate),
                              help="Baudrate of serial port. ENV: SERIAL_BAUDRATE")

    serial_group.add_argument("--parity", "-P", dest="serial_parity", type=SerialParity,
                              default=getenv("SERIAL_PARITY", default_config.serial.parity),
                              help="Parity. N for None, E for Even, O for Odd, M for Mark, S for Space. "
                                   "ENV: SERIAL_PARITY")

    serial_group.add_argument("--databits", "-d", dest="serial_databits", type=int, choices=[5, 6, 7, 8],
                              default=getenv("SERIAL_DATABITS", default_config.serial.databits),
                              help="Number of data bits. ENV: SERIAL_DATABITS")

    serial_group.add_argument("--stopbits", "-s", dest="serial_stopbits", type=float, choices=[1, 1.5, 2],
                              default=getenv("SERIAL_STOPBITS", default_config.serial.stopbits),
                              help="Number of stop bits. ENV: SERIAL_STOPBITS")

    mqtt_group = parser.add_argument_group("mqtt")

    mqtt_group.add_argument("--mqtt.host", "--host", "-h", dest="mqtt_host", type=str,
                            default=getenv("MQTT_HOST", default_config.mqtt.host),
                            help="Hostname of MQTT broker. ENV: MQTT_HOST")

    mqtt_group.add_argument("--mqtt.port", "-p", dest="mqtt_port", type=int,
                            default=getenv("MQTT_PORT", default_config.mqtt.port),
                            help="Port of MQTT broker. ENV: MQTT_PORT")

    mqtt_group.add_argument("--mqtt.user", "--user", "-u", dest="mqtt_user", type=str,
                            default=getenv("MQTT_USER", default_config.mqtt.user),
                            help="User to use for connecting to MQTT. ENV: MQTT_USER")

    mqtt_group.add_argument("--mqtt.password", "--password", dest="mqtt_password", type=str,
                            default=getenv("MQTT_PASSWORD", default_config.mqtt.password),
                            help="Password to use for connecting to MQTT. ENV: MQTT_PASSWORD")

    mqtt_group.add_argument("--mqtt.client_id", "--client_id", "-c", dest="mqtt_client_id", type=str,
                            default=getenv("MQTT_CLIENT_ID", default_config.mqtt.client_id),
                            help="Client ID to use for connecting to MQTT. ENV: MQTT_CLIENT_ID")

    mqtt_group.add_argument("--mqtt.topic", "--topic", "-t", dest="mqtt_topic", type=str,
                            default=getenv("MQTT_TOPIC", default_config.mqtt.topic),
                            help="Topic name to use. Will expand to cmnd/{topic}/{command} and stat/{topic}/{command}. "
                                 "ENV: MQTT_TOPIC")
    mqtt_group.add_argument("--mqtt.stat_interval", "--stat_interval", "-i", dest="mqtt_stat_interval", type=int,
                            default=getenv("MQTT_STAT_INTERVAL", default_config.mqtt.stat_interval),
                            help="Interval (in seconds) how often output stats are pushed to MQTT regardless of request / change of outputs. "
                                 "ENV: MQTT_STAT_INTERVAL")

    args = parser.parse_args()

    if args.help:
        parser.print_help()
        sys.exit(1)

    return Config(
        serial=SerialConfig(
            port=args.serial_port,
            baudrate=args.serial_baudrate,
            parity=args.serial_parity,
            databits=args.serial_databits,
            stopbits=args.serial_stopbits
        ),
        mqtt=MqttConfig(
            host=args.mqtt_host,
            port=args.mqtt_port,
            user=args.mqtt_user or None,
            password=args.mqtt_password or None,
            client_id=args.mqtt_client_id or None,
            topic=args.mqtt_topic,
            stat_interval=args.mqtt_stat_interval,
        ),
        debug=args.debug
    )


def main():
    logging.captureWarnings(True)

    config = get_config()

    logging.basicConfig(
        format="%(asctime)s %(levelname)s %(name)s: %(message)s",
        level=logging.DEBUG if config.debug else logging.INFO
    )
    logging.debug("Debug mode enabled.")

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    try:
        sys.exit(loop.run_until_complete(app(config)))
    except Exception as exc:
        logging.exception(str(exc))
        sys.exit(1)


if __name__ == "__main__":
    main()
